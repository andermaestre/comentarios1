USE [DAM_Andermaestre_DEV]
GO
/****** Object:  Schema [Comentarios]    Script Date: 18/02/2020 13:13:21 ******/
CREATE SCHEMA [Comentarios]
GO
/****** Object:  Table [Comentarios].[Comentarios]    Script Date: 18/02/2020 13:13:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Comentarios].[Comentarios](
	[IdComentario] [int] NOT NULL,
	[IdUsuario] [int] NULL,
	[Texto] [varchar](50) NULL,
	[IdDestinatario] [int] NULL,
 CONSTRAINT [PK_Comentarios] PRIMARY KEY CLUSTERED 
(
	[IdComentario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [Comentarios].[Usuarios]    Script Date: 18/02/2020 13:13:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [Comentarios].[Usuarios](
	[IdUsuario] [int] NOT NULL,
	[Clave] [varchar](50) NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[Foto] [image] NULL,
 CONSTRAINT [PK_Usuarios_2] PRIMARY KEY CLUSTERED 
(
	[IdUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [Comentarios].[Comentarios] ([IdComentario], [IdUsuario], [Texto], [IdDestinatario]) VALUES (1, 1, N'Hola', 2)
INSERT [Comentarios].[Comentarios] ([IdComentario], [IdUsuario], [Texto], [IdDestinatario]) VALUES (2, 2, N'Hey', 1)
INSERT [Comentarios].[Comentarios] ([IdComentario], [IdUsuario], [Texto], [IdDestinatario]) VALUES (3, 4, N'Nonono', 3)
INSERT [Comentarios].[Comentarios] ([IdComentario], [IdUsuario], [Texto], [IdDestinatario]) VALUES (4, 3, N'Que Dices', 2)
INSERT [Comentarios].[Comentarios] ([IdComentario], [IdUsuario], [Texto], [IdDestinatario]) VALUES (5, 3, N'A ti te decia', 4)
INSERT [Comentarios].[Usuarios] ([IdUsuario], [Clave], [Nombre], [Apellido], [Foto]) VALUES (1, N'1234', N'Javi', N'Torres', NULL)
INSERT [Comentarios].[Usuarios] ([IdUsuario], [Clave], [Nombre], [Apellido], [Foto]) VALUES (2, N'1234', N'Sara', N'Casas', NULL)
INSERT [Comentarios].[Usuarios] ([IdUsuario], [Clave], [Nombre], [Apellido], [Foto]) VALUES (3, N'1234', N'Claro', N'Pueblos', NULL)
INSERT [Comentarios].[Usuarios] ([IdUsuario], [Clave], [Nombre], [Apellido], [Foto]) VALUES (4, N'1234', N'Mario', N'Barrios', NULL)
INSERT [Comentarios].[Usuarios] ([IdUsuario], [Clave], [Nombre], [Apellido], [Foto]) VALUES (5, N'1234', N'Juana', N'Ciudades', NULL)
ALTER TABLE [Comentarios].[Comentarios]  WITH CHECK ADD  CONSTRAINT [FK_Comentarios_Usuarios] FOREIGN KEY([IdUsuario])
REFERENCES [Comentarios].[Usuarios] ([IdUsuario])
GO
ALTER TABLE [Comentarios].[Comentarios] CHECK CONSTRAINT [FK_Comentarios_Usuarios]
GO
